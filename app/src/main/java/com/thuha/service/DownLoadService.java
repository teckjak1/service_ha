package com.thuha.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class DownLoadService extends Service implements MyTask.OnAsyncCallBack {
    public static final String TAG = "TAG";
    public static final int KEY_DOWNLOAD = 1;
    public static final String ACTION_SHOW_IMAGE = "show Image";
    public static final String ACTION_UPDATE_PROGRESS = "progress";
    public static final String KEY_PROGRESS = "key";
    private String savePath;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        download(intent);
        return START_NOT_STICKY;
    }

    private void download(Intent intent) {
        if (intent == null) {
            Toast.makeText(this, "Intent is null, could download anymore", Toast.LENGTH_SHORT).show();
            stopSelf();
            return;
        }
        String link = intent.getStringExtra(MainActivity.KEY_LINK);
        new MyTask(this, KEY_DOWNLOAD).execute(link);
    }


    @Override
    public Object execTask(MyTask task, int key, Object... data) {
        String link = (String) data[0];
        if (link == null) {
            return false;
        }
//https://upload.wikimedia.org/wikipedia/commons/7/73/Lion_waiting_in_Namibia.jpg
        try {
            URLConnection url = new URL(link).openConnection();
            InputStream in = url.getInputStream();
            int totalSize = url.getContentLength();
            //đường dẫn bộ nhớ cộng tên file cần ghi ra
            savePath = getExternalFilesDir(null).getPath() + "/" + new File(link).getName();
            File fileOut = new File(savePath);
            FileOutputStream out = new FileOutputStream(fileOut);

            byte[] buff = new byte[1024];
            int len = in.read(buff);
            int currentSize = 0;
            while (len > 0) {
                out.write(buff, 0, len);
                currentSize += len;
                Log.i("TAG", "" + (currentSize * 100 / totalSize));
                //update progress
                task.updateTask((currentSize * 100 / totalSize));
                len = in.read(buff);
            }
            in.close();
            out.close();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void updateUI(int key, Object... values) {
        //send broadcast to act
        Intent intent = new Intent(ACTION_UPDATE_PROGRESS);
        intent.putExtra(KEY_PROGRESS, (int) values[0]);
        sendBroadcast(intent);
    }

    @Override
    public void taskComplete(int key, Object data) {
        if (!((boolean) data)) {
            Toast.makeText(this, "Download fail", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Download succes", Toast.LENGTH_SHORT).show();
        }

        stopSelf();
        //send broadcast to act
        Intent intent = new Intent(ACTION_SHOW_IMAGE);
        intent.putExtra(MainActivity.KEY_LINK, savePath);
        sendBroadcast(intent);


    }

}
