package com.thuha.service;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_LINK = "KEY_LINK";
    private EditText edtLink;
    private ImageView imgPhoto;
    private BroadcastReceiver receiver;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        initView();
    }

    private void initView() {
        edtLink = findViewById(R.id.edt_link);
        findViewById(R.id.bt_download).setOnClickListener(this);
        imgPhoto = findViewById(R.id.iv_photo);
        // tạo tổng đài
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                doReceive(intent);
            }
        };
        // Register receiver to handle action;
        IntentFilter filter = new IntentFilter();
        filter.addAction(DownLoadService.ACTION_SHOW_IMAGE);
        filter.addAction(DownLoadService.ACTION_UPDATE_PROGRESS);
        registerReceiver(receiver, filter);


    }

    @Override
    protected void onDestroy() {
        //hủy đăng kí
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private void doReceive(Intent intent) {
        Toast.makeText(this, "Download helo", Toast.LENGTH_SHORT).show();

        if (intent == null || intent.getAction() == null) {
            return;
        }
        if (intent.getAction().equals(DownLoadService.ACTION_SHOW_IMAGE)) {
            String link = intent.getStringExtra(KEY_LINK);
            Bitmap bitmap = BitmapFactory.decodeFile(link);
            imgPhoto.setImageBitmap(bitmap);
        } else if (intent.getAction().equals((DownLoadService.ACTION_UPDATE_PROGRESS))) {
            int progress = intent.getIntExtra(DownLoadService.KEY_PROGRESS, 0);
            Log.i("PROGREE",progress+"");
            TextView text = findViewById(R.id.tv_progress);
            text.setText(progress + "");
            progressBar = findViewById(R.id.progress);
            progressBar.setProgress(progress);
        }


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_download) {
            downLoad();
        }
    }

    private void downLoad() {
        if (edtLink.getText().toString().isEmpty()) {
            Toast.makeText(this, "Link is empty", Toast.LENGTH_SHORT).show();
            return;
        }
//start 1 Service để thực hiện download
        Intent intent = new Intent();
        intent.setClass(this, DownLoadService.class);
        intent.putExtra(KEY_LINK, edtLink.getText().toString());
        startService(intent);
    }
}
